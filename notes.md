- each level have 3 sets of same difficult

  - easy:
    - level up with 2 of 3 corrects
    - no level down
  - normal:
    - level up with 2 of 3 corrects
    - player have 3 lives, each retry on same level takes 1 live
    - when the player run out of lives moves back 1 level
    - the lives resets on level up
  - hard:
    - same as normal but need 3/3 corrects to pass
  - extreme:
    - same as hard but with only 1 live

- calculate difficulties

  - defs:

    - distance: the minimum quantity difference between the max quantity and the second max quantity
    - NOFAIL: clicking the wrong tile do nothing

  - levels:

- DISTANCE= the minimum quantity difference between the max quantity and the second max quantity

- lvl 1: READY TUTORIAL
- lvl 2: 1 TILE
- lvl 3: 2 TILES / 1 EMPTY / NOFAIL
- lvl 4: 2 TILES / 1 EMPTY

- 5 to 12: 2 TILES / distance <- 8 to 1

- 13 to 20: 3 TILES / 1 EMPTY / distance <- 8 to 1
- 21 TO 28: 3 TILES / distance <- 8 to 1

- 29 TO 35: 4 TILES / 1 EMPTY / distance <- 7 to 1
- 36 TO 42: 4 TILES / distance <- 7 to 1
