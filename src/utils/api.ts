import { state } from './stores';

const baseUrl = 'https://backend.incalab.cl';

export const login = async (username: string, password: string) => {
  const body = JSON.stringify({ username, password });
  const response = await fetch(new URL('/account/login/', baseUrl), {
    method: 'POST',
    headers: new Headers({
      'Content-Length': body.length.toString(),
      'Content-Type': 'application/json',
    }),
    body,
  });

  if (response.ok) {
    const result = await response.json();
    localStorage.setItem('token', result.token);
    state.login();
  }
};

// export const logout = async () => {
//   const token = localStorage.getItem('token');
//   if (token) {
//     const response = await fetch(new URL('/account/login/', baseUrl), {
//       method: 'POST',
//       headers: new Headers({
//         'Authorization': `Token ${token}`,
//       }),
//       body,
//     });
  
//   }
//   if (response.ok) {
//     const result = await response.json();
//     localStorage.setItem('token', result.token);
//     state.login();
//   }
// };

// export const logCreate = async () => {
//   const response = await fetch('', {
//     method: 'POST',
//     headers: {
//       'Content-Length': body.length,
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({

//     }),
//   });
// };
