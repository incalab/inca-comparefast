import { writable } from 'svelte/store';
import defaultData from './defaultData.json';
import type { Mode } from 'inca-utils';
import type { State, View, Config, Logs, Log, Campaign } from '../vite-env';

const saveDelay = 2000;

const createState = () => {
  const loggedIn = !!localStorage.getItem('token');

  const { subscribe, update } = writable<State>({
    view: 'menu',
    loggedIn,
  });

  return {
    subscribe,
    switchView: (view: View, params?: { mode: Mode }) =>
      update(value => ({ ...value, view, params: params || value.params })),
    login: () => update(value => ({ ...value, loggedIn: true })),
    logout: () => update(value => ({ ...value, loggedIn: false })),
  };
};

const createConfig = () => {
  const localStorageKey = 'comparefast-config';
  const storedData = localStorage.getItem(localStorageKey);
  const parsedData = storedData && JSON.parse(storedData);
  const data =
    parsedData?.version && parsedData.version === defaultData.config.version
      ? parsedData
      : defaultData.config;

  const { subscribe, set, update } = writable<Config>(data);

  let timeout: string | number | NodeJS.Timeout;
  subscribe(data => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      localStorage.setItem(localStorageKey, JSON.stringify(data));
    }, saveDelay);
  });

  return {
    subscribe,
    set,
    update,
  };
};

const createCampaign = () => {
  const localStorageKey = 'comparefast-campaign';
  const storedData = localStorage.getItem(localStorageKey);
  const parsedData = storedData && JSON.parse(storedData);
  const data =
    parsedData?.version && parsedData.version === defaultData.campaign.version
      ? parsedData
      : defaultData.campaign;

  const { subscribe, set, update } = writable<Campaign>(data);

  let timeout: string | number | NodeJS.Timeout;
  subscribe(data => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      localStorage.setItem(localStorageKey, JSON.stringify(data));
    }, saveDelay);
  });

  return {
    subscribe,
    set,
    update,
  };
};

const createLogger = () => {
  const localStorageKey = 'comparefast-logs';
  const storedData = localStorage.getItem(localStorageKey);
  const data = (storedData && JSON.parse(storedData)) || {};

  const { subscribe, update } = writable<Logs>(data);

  let timeout: string | number | NodeJS.Timeout;
  subscribe(data => {
    // here we can send logs to an API

    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      localStorage.setItem(localStorageKey, JSON.stringify(data));
    }, saveDelay);
  });

  return {
    subscribe,
    log(subject: string, data: Log) {
      update(logs => {
        if (!logs[subject]) logs[subject] = [];
        return {
          ...logs,
          [subject]: [...logs[subject], data],
        };
      });
    },
  };
};

export const state = createState();
export const config = createConfig();
export const campaign = createCampaign();
export const logger = createLogger();
