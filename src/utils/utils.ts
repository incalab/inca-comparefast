import { Parser } from '@json2csv/plainjs';
import type { Logs } from '../vite-env';
import JSZip from 'jszip';

/**
 * Get a random int between min and max
 * @param min Lower boundary inclusive
 * @param max Upper boundary inclusive
 * @returns a random integer
 */
export const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max + 1);
  return Math.floor(Math.random() * (max - min) + min);
};

export const getNDifferentRandomInts = (
  min: number,
  max: number,
  n: number
) => {
  const availableInts = Array.from(
    { length: max - min + 1 },
    (_, i) => i + min
  );
  const results: number[] = [];
  for (let i = 0; i < n; i++) {
    const randomIndex = getRandomInt(0, availableInts.length - 1);
    results.push(availableInts.splice(randomIndex, 1)[0]);
  }
  return results;
};

/**
 * Shuffle an array
 * @param array the array
 * @returns the array shuffled
 */
export const shuffleArray = (array: unknown[]) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

export const asyncLogsToCsvUrl2 = async (logObject: Logs) => {
  const zip = new JSZip();
  const csvs = {};
  for (const [subject, logs] of Object.entries(logObject)) {
    if (!logs.length) continue;
    let csv = '';
    const last = (i: number) => i === Object.keys(logs[0]).length - 1;
    Object.keys(logs[0]).forEach((key, i) => {
      csv += `${key}${last(i) ? '\n' : ','}`;
    });

    logs.forEach(log => {
      Object.values(log).forEach((value, i) => {
        csv += `${JSON.stringify(value)}${last(i) ? '\n' : ','}`;
      });
    });

    csvs[subject] = csv;
    zip.file(`${subject}.csv`, new Blob([csv], { type: 'text/csv' }));
  }

  const content = await zip.generateAsync({ type: 'blob' });
  return URL.createObjectURL(content);
};

export const asyncLogsToCsvUrl = async (logObject: Logs) => {
  const parser = new Parser({});
  const zip = new JSZip();
  for (const [subject, logs] of Object.entries(logObject)) {
    const csv = parser.parse(logs);
    zip.file(`${subject}.csv`, new Blob([csv], { type: 'text/csv' }));
  }
  const content = await zip.generateAsync({ type: 'blob' });
  return URL.createObjectURL(content);
};

export const logsToJSONUrl = (logObject: Logs) => {
  return URL.createObjectURL(
    new Blob([JSON.stringify(logObject, null, 2)], {
      type: 'application/json',
    })
  );
};
