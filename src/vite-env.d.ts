/// <reference types="svelte" />
/// <reference types="vite/client" />

import type { Mode } from 'inca-utils';

interface Config {
  version: string;
  profile: {
    learnerName: string;
    instructorName: string;
  };
  difficulty: {
    sublevelsNumber: number;
    livesNumber: number;
    correctsToLevelUp: number;
    judgeTime: number;
  };
  others: {
    displayedModes;
  };
  gameUI: {
    displayLives: boolean;
    displayLevelAnswers: boolean;
    displayLevelResult: boolean;
    displayTrainerButtons: boolean;
  };
}

type View = 'menu' | 'campaign' | 'about' | 'settings' | 'login';

interface State {
  view: View;
  params?: {
    mode: Mode;
  };
  loggedIn: boolean;
}

interface Campaign {
  version: string;
  [mode: Mode]: {
    level: number;
  };
}

interface Log {
  answer: boolean;
  chosenQuantity: number;
  mode: Mode;
  backgroundColor: string;
  foregroundColor: string;
  tileBackgroundColor: string;
  tileForegroundColor: string;
  tileBorderColor: string;
  gridSize: number;
  quantities: number[];
  time: number;
  date: string;
}

interface Logs {
  [subjectName: string]: Log[];
}
